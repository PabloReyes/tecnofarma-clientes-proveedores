window.addEventListener('DOMContentLoaded', function(){
    // CONFIGURAR LIBRERÍAS
    moment.locale('es');
    
    // INIT
    if(document.querySelector('#formulario')) initHomePage();
    if(document.querySelector('#innerpage')) initInnerPage();
});

// HOME PAGE FUNCTIONS
function initHomePage() {
    // CLEAR LOCALSTORAGE
    localStorage.removeItem('tcnfrm-data');
    // DOM elements
    const homeSelect = document.querySelectorAll('[data-select=home]');
    const btnContinue = document.querySelector('#btn-continue');

    initCurrentDate();
    initHomeListeners(homeSelect);
    initContinueAction( btnContinue, homeSelect );
}
function initCurrentDate(){
    const now = moment();
    document.querySelector('#fecha-hoy-display').textContent = now.format('D [de] MMMM [del] YYYY');
    document.querySelector('#fecha-hoy').value = now.format('YYYYMMDD');
}
function initHomeListeners(homeSelect) {
    const tipoCliente = homeSelect.item(2);
    homeSelect.forEach(function(select){
        select.addEventListener('change', function(e){
            const target = e.target;
            const name = target.name;
            const value = target.value;
            if(name === 'tipo_usuario'){
                checkUserTypeSelection(value, tipoCliente);
            }
            if( target.hasAttribute('required') && value !== ''){
                target.removeAttribute('required');
            }
        });
    });
}
function checkUserTypeSelection(value,tipoCliente){
    tipoCliente.value = '';
    if( value === '' ) {
        tipoCliente.setAttribute('disabled', 'true');
    } else {
        tipoCliente.removeAttribute('disabled');
        if( value === 'proveedor') {
            document.querySelectorAll('.proveedor_only').forEach( node => node.classList.remove('hidden'));
        } else {
            document.querySelectorAll('.proveedor_only').forEach( node => node.classList.add('hidden'));
        }
    }
}
function initContinueAction(btn, homeSelect){
    const selectMap = {};
    let validated = false;
    btn.addEventListener('click', function(){
        homeSelect.forEach( function(select){
            const name = select.name;
            const value = select.value;
            selectMap[name] = value;
            if( value === '' ) {
                //selectMap['isValid'] = false;
                select.setAttribute('required', true);
            } else {
                //selectMap['isValid'] = true;
                select.removeAttribute('required');
            }
        });

        validated =  selectMap['tipo_cliente'] !== '' && selectMap['tipo_tramite'] !== '' && selectMap['tipo_usuario'] !== '';

        if( validated ){
            // STORE IN LOCAL STORAGE
            const data = {
                'fecha': moment().format('YYYYMMDD'),
                'tipo_cliente': selectMap['tipo_cliente'],
                'tipo_tramite': selectMap['tipo_tramite'],
                'tipo_usuario': selectMap['tipo_usuario'],
            }
            console.log(data);
            localStorage.setItem('tcnfrm-data', JSON.stringify(data));
            window.location.href = `./${selectMap['tipo_usuario']}/${selectMap['tipo_cliente']}.html`;
        }
    });
}

// INNER PAGE FUNCTIONS
function initInnerPage() {
    initMetaFields();
    initCollapsibles();
    initDynamicFields();
    initModal();
}

function initMetaFields() {
    // USER MAP
    const userMap = {
        "vinculacion": "vinculación",
        "actualizacion": "actualización",
        "cliente": "cliente",
        "proveedor": "proveedor",
        "natural": "persona natural",
        "juridica": "persona jurídica",
        "internacional": "persona internacional",
        "consultor": "Consultor / Speaker Internacional - Nacional"
    }
    // RECUPERAR DE LOCAL STORAGE
    const storedData = JSON.parse(localStorage.getItem("tcnfrm-data"));
    const inputFecha = document.querySelector('#fecha');
    const inputTipoCliente = document.querySelector('#tipo_cliente');
    const inputTipoTramite = document.querySelector('#tipo_tramite');
    const inputTipoUsuario = document.querySelector('#tipo_usuario');
    const formTitle = document.querySelector('#form-title');

    inputFecha.value = storedData['fecha'];
    inputTipoCliente.value = storedData['tipo_cliente'];
    inputTipoTramite.value = storedData['tipo_tramite'];
    inputTipoUsuario.value = storedData['tipo_usuario'];

    formTitle.textContent = `Formulario de ${userMap[storedData['tipo_tramite']]} de ${userMap[storedData['tipo_usuario']]} - ${userMap[storedData['tipo_cliente']]}`;
}

function initCollapsibles(){
    const triggers = document.querySelectorAll("[data-collapsible-trigger]");
    triggers.forEach(function(element){
        const target = element.dataset['collapsibleTrigger'];
        const targetElement = document.querySelector(`[data-collapsible-target=${target}]`);
        element.addEventListener('change', function(){
            if( targetElement.classList.contains('h-0') ){
                // show
                targetElement.classList.replace('h-0','h-auto');
                targetElement.classList.replace('overflow-hidden','overflow-auto');
            } else {
                // hide
                targetElement.classList.replace('h-auto','h-0');
                targetElement.classList.replace('overflow-auto', 'overflow-hidden');
            }
        });
    });
}

function initDynamicFields(){
    // ENCONTRAR LOS CONTENEDORES DINAMICOS
    let initial_index = 0;
    let total_fields = 1;
    const max_fields = 10;
    const container = document.querySelector('[data-dynamic="container"]');
    if( !container ) return;
    const add_fields_btn = container.querySelector('#dynamic-container-add');
    const fields_container = container.querySelector('#dynamic-fields-container');
    const fields = [
        {'id':'composicion_accionaria','key':'tipo_doc','label':'Tipo doc'},
        {'id':'composicion_accionaria','key':'num_doc','label':'Nº de documento'},
        {'id':'composicion_accionaria','key':'nombre_accionista','label':'Nombre accionista'},
        {'id':'composicion_accionaria','key':'porcentaje','label':'Porcentaje'},
        {'id':'composicion_accionaria','key':'ciudad_pais','label':'Ciudad/País'},
        {'id':'composicion_accionaria','key':'peps','label':'PEPs'},
    ]

    // EVENTS
    add_fields_btn.addEventListener('click', function(){
        if( total_fields <= max_fields ){
            const next_index = initial_index + 1;
            // Crear los campos
            const fieldset = createFieldset();
            const closeButton = createCloseButton(next_index);
            
            fields_container.appendChild(fieldset);
            fields.forEach(function(field){
                const element = createFormField(field,next_index);
                fieldset.appendChild(element);
            });
            fieldset.appendChild(closeButton);
            initial_index = next_index;
            total_fields++;
        }
    });
    container.addEventListener('click', function(e){
        let target = e.target;
        if( target.nodeName === 'IMG' ) target = e.target.parentElement;
        if( 'dynamic' in target.dataset ){
            const fieldset = target.parentElement.parentElement;
            fieldset.remove();

            updateIndexes(fields_container.children, fields)

            total_fields--;
        }
    });
}
// DOM CREATION
function createFieldset(){
    const fieldset = document.createElement('div');
    fieldset.classList.add('form-fieldset', 'w-full', 'grid-cols-13');
    return fieldset;
}
function createCloseButton( elementIndex ){
    const div = document.createElement('div');
    const button = document.createElement('button');
    const img = document.createElement('img');

    div.classList.add("self-center", "justify-self-center");
    button.setAttribute('type','button');
    button.classList.add('pt-5','opacity-25','hover:opacity-75');
    button.dataset.dynamic = elementIndex;
    img.classList.add('block','w-5','h-5');
    img.src = '../../assets/images/icon-minus.svg';

    button.appendChild(img);
    div.appendChild(button);

    return div;
}
function createFormField(data, elementIndex){
    const elementID = `${data.id}_${elementIndex}_${data.key}`;
    const elementName = `${data.id}[${elementIndex}][${data.key}]`;
    const div = document.createElement('div');
    const label = document.createElement('label');
    const input = document.createElement('input');

    div.classList.add('form-field','col-span-2');
    label.setAttribute('for',elementID);
    label.classList.add('form-label');
    label.textContent = data.label;
    input.setAttribute('type','text');
    input.value = "";
    input.name = elementName;
    input.id = elementID;

    div.appendChild(label);
    div.appendChild(input);

    return div;
}
function updateIndexes( elements, fields ){
    Array.from(elements).forEach(function(element, index){
        const formFields = element.querySelectorAll('.form-field');
        Array.from(formFields).forEach(function(field, fieldIndex){
            const label = field.querySelector('label');
            const input = field.querySelector('input');
            const elementID = `${fields[fieldIndex].id}_${index}_${fields[fieldIndex].key}`;
            label.setAttribute('for', elementID );
            input.name = `${fields[fieldIndex].id}[${index}][${fields[fieldIndex].key}]`;
            input.id = elementID;
        });
    });
}

function initModal(){
    const trigger = document.querySelector('#declaracion-autorización-trigger');
    const modal = document.querySelector('#declaracion-autorización-modal');
    const panelOrigenFondos = document.querySelector('#modal-origen-fondos');
    const panelTratamientoDatos = document.querySelector('#modal-tratamiento-datos');
    const triggerOrigen = document.querySelector('#panel-trigger-origen');
    const triggerTratamiento = document.querySelector('#panel-trigger-tratamiento');

    trigger.addEventListener('click', function(){
        if( modal.dataset.modal === 'closed' ){
            delete modal.dataset.modal;
        }
    });
    modal.addEventListener('click', function(e){
        const target = e.target;
        if(target.id === 'declaracion-autorización-modal' || target.id === 'close-modal' || target.id === 'btn-cerrar-modal'){
            modal.dataset.modal = 'closed'
        }
        // if(target.dataset.target === 'modal-origen-fondos'){
        //     if(panelOrigenFondos.dataset.panel === "closed"){
        //         panelOrigenFondos.dataset.panel = 'open';
        //         panelTratamientoDatos.dataset.panel = "closed";
        //         triggerOrigen.dataset.status = "open";
        //         triggerTratamiento.dataset.status = "closed";
        //     }
        // }
        // if(target.dataset.target === 'modal-tratamiento-datos'){
        //     if(panelTratamientoDatos.dataset.panel === "closed"){
        //         panelTratamientoDatos.dataset.panel = 'open';
        //         panelOrigenFondos.dataset.panel = "closed";
        //         target.dataset.status === 'closed';
        //         triggerTratamiento.dataset.status = "open";
        //         triggerOrigen.dataset.status = "closed";
        //     }
        // }
    });
}