# Tecnofarma - Creación o actualización de clientes y proveedores

- [Tecnofarma - Creación o actualización de clientes y proveedores](#tecnofarma---creación-o-actualización-de-clientes-y-proveedores)
  - [Manejo de estilos CSS de formularios](#manejo-de-estilos-css-de-formularios)
    - [Estructura de archivos CSS](#estructura-de-archivos-css)
    - [Requerimientos Tailwind CSS](#requerimientos-tailwind-css)
    - [Inicializar Tailwind CSS](#inicializar-tailwind-css)
  - [Campos obligatorios](#campos-obligatorios)
  - [Activar / desactivar modo testeo](#activar--desactivar-modo-testeo)
  - [Estructura de páginas](#estructura-de-páginas)
    - [Clientes: Persona natural](#clientes-persona-natural)
    - [Clientes: Persona jurídica](#clientes-persona-jurídica)
    - [Proveedores: Persona natural](#proveedores-persona-natural)
    - [Proveedores: Persona jurídica](#proveedores-persona-jurídica)
    - [Proveedores: Persona internacional](#proveedores-persona-internacional)
  - [Formularios](#formularios)
    - [Información base](#información-base)
    - [Clientes](#clientes)
      - [Campos comunes](#campos-comunes)
      - [Campos cliente persona natural](#campos-cliente-persona-natural)
      - [Campos cliente persona jurídica](#campos-cliente-persona-jurídica)
    - [Proveedores](#proveedores)
      - [Campos proveedores persona natural](#campos-proveedores-persona-natural)
      - [Campos proveedores persona jurídica](#campos-proveedores-persona-jurídica)
      - [Campos proveedores persona internacional](#campos-proveedores-persona-internacional)

---

## Manejo de estilos CSS de formularios

Las páginas están estiladas a través de [Tailwind CSS](https://tailwindcss.com/).

A continuación se detalla el procedimiento para trabajar con el Framework

### Estructura de archivos CSS

Existen dos archivos principales utilizados por Tailwind:

- ./src/input.css

  Archivo que contiene las refenrecias a las reglas CSS de Tailwind.

- ./dist/styles.css

  Archivo compilado con todas las reglas CSS implementadas desde los archivos HTML

> **IMPORTANTE**
>
> Ninguno de estos archivos debería requerir modificaciones. En los casos que lo requiriera, revisar [documentación de Tailwind CSS](https://tailwindcss.com/docs/installation)

### Requerimientos Tailwind CSS

- Node
- NPM

### Inicializar Tailwind CSS

Para iniciar el sitio en modo desarrollo utilizar:

`npm run dev`

En caso de hacer modificaciones en el diseño y estructuras HTML y/o CSS, es necesario inicializar el proceso de _watch_ para generar el archivo de _output_ a medida que se vayan haciendo cambios.

Para esto utilizar:

`npx tailwindcss -i ./src/input.css -o ./dist/styles.css --watch`

## Campos obligatorios

Para mostrar un campo que requiere ser llenado por el usuario tras validar, es necesario agregar al elemento HTML el atributo _required_.

En el siguiente ejemplo el campo de texto contiene el atributo _required_, al estár presente este atributo, el mensaje ubicado en el elemento _span_ que viene justo a continuación se hará visible.

```html
<input
  type="text"
  value=""
  name="inf_fina_otros_ingresos"
  id="inf_fina_otros_ingresos"
  class="peer required"
  required
/>
<span class="error-message">Por favor seleccione una de las opciones</span>
```

## Activar / desactivar modo testeo

Cada página de formulario incluye un pequeño código que permite testear los valores que entrega el formulario. Ese código _bloquea el envío de los datos_ por lo tanto debe comentarse o eliminarse antes de pasar a producción o ser conectado a un backend.

El código es el siguiente y está ubicado al final de cada página de formularios.

```html
<!--SOLO PARA TEST-->
<script
  src="https://code.jquery.com/jquery-3.7.0.min.js"
  integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g="
  crossorigin="anonymous"
></script>
<script>
  $("#form").on("submit", function (e) {
    e.preventDefault();
    const post = $("#form").serialize();
    const h = JSON.parse(
      '{"' +
        decodeURI(post)
          .replace(/"/g, '\\"')
          .replace(/&/g, '","')
          .replace(/=/g, '":"') +
        '"}'
    );
    console.log(h);
  });
</script>
<!--SOLO PARA TEST-->
```

## Estructura de páginas

En esta sección se describen las secciones para la navegación de cada página.

### Clientes: Persona natural

- Datos generales
- Información básica
- Información tributaria
- Representación legal
- Información financiera
- Referencia bancaria
- Referencias comerciales
- Personas de contacto
- Carga de archivos
- Declaraciones y autorizaciones

### Clientes: Persona jurídica

- Datos generales
- Relación entidad
- Información tributaria
- Representación legal
- Revisor fiscal
- Socios y accionistas
- Información financiera
- Referencia bancaria
- Referencias comerciales
- Personas de contacto
- Carga de archivos
- Declaraciones y autorizaciones

### Proveedores: Persona natural

- Datos generales
- Información tributaria
- Representación legal
- Información financiera
- Referencia bancaria
- Referencias comerciales
- Personas de contacto
- Carga de archivos
- Declaraciones y autorizaciones

### Proveedores: Persona jurídica

- Datos generales
- Relación entidad
- Información tributaria
- Representación legal
- Revisor fiscal
- Socios y accionistas
- Información financiera
- Referencia bancaria
- Referencias comerciales
- Personas de contacto
- Carga de archivos
- Declaraciones y autorizaciones

### Proveedores: Persona internacional

- Datos generales
- Relación entidad
- Información tributaria
- Representación legal
- Revisor fiscal
- Socios y accionistas
- Información financiera
- Referencia bancaria
- Referencias comerciales
- Personas de contacto
- Carga de archivos
- Declaraciones y autorizaciones

## Formularios

En esta sección están descritos los campos que componen cada formulario, en particular su atributo html _"name"_, el cual puede ser referenciado desde el objeto **POST** enviado al _endpoint_ del servidor.

### Información base

Todos los formularios, de cliente y de proveedores llevan adjuntos los siquientes datos:

| Datos globales                 |              |                                    |
| ------------------------------ | ------------ | ---------------------------------- |
| **Etiqueta**                   | **name**     | **formato / valores**              |
| Nombre o razón social completo | fecha        | "YYYYMMDD"                         |
| Tipo de trámite                | tipo_tramite | [vinculacion, actualizacion]       |
| Tipo de usuario                | tipo_usuario | [cliente, proveedor]               |
| Tipo de cliente                | tipo_cliente | [natural, juridica, internacional] |

### Clientes

Los clientes están agrupados en **persona natural** y **persona jurídica**. Cada formulario es independiente pero contiene campos comunes, los que serán marcados en esta documentación en una sección común.

#### Campos comunes

| Datos generales                |                    |
| ------------------------------ | ------------------ |
| **Etiqueta**                   | **name**           |
| Nombre o razón social completo | nombre_completo    |
| Código CIIU                    | codigo_ciiu        |
| Tipo de identificación         | tipo_id            |
| Número de identificación       | numero_id          |
| Tipo de actividad económica    | tipo_actividad     |
| Día fecha de constitución      | const_dia          |
| Mes fecha de constitución      | const_mes          |
| Año fecha de constitución      | const_anio         |
| Nº registro mercantil          | numero_registro    |
| Objeto social                  | objeto_social      |
| Correo facturación electrónica | correo_facturacion |
| Direccion                      | direccion          |
| Teléfono                       | telefono           |
| Ciudad                         | ciudad             |
| Correo Electronico             | correo             |

| Información financiera                                     |                           |
| ---------------------------------------------------------- | ------------------------- |
| **Etiqueta**                                               | **name**                  |
| Día fecha de corte                                         | inf_fina_corte_dia        |
| Mes fecha de corte                                         | inf_fina_corte_mes        |
| Año fecha de corte                                         | inf_fina_corte_anio       |
| Total activos                                              | inf_fina_total_activos    |
| Total ingresos                                             | inf_fina_total_ingresos   |
| Total costos y gastos                                      | inf_fina_costos_gastos    |
| Total pasivos                                              | inf_fina_total_pasivos    |
| Total patrimonio                                           | inf_fina_total_patrimonio |
| Detalle otros ingresos diferentes a la actividad principal | inf_fina_otros_ingresos   |
| Valor otros ingresos                                       | inf_fina_valor_otros      |

| Referencia bancaria |                           |
| ------------------- | ------------------------- |
| **Etiqueta**        | **name**                  |
| Nombre              | ref_bancaria_nombre       |
| Dirección           | ref_bancaria_direccion    |
| Ciudad              | ref_bancaria_ciudad       |
| Teléfono            | ref_bancaria_telefono     |
| Nº de producto      | ref_bancaria_num_producto |

| Referencia comercial | (Obligatorio dos)            |
| -------------------- | ---------------------------- |
| **Etiqueta**         | **name**                     |
| Nombre               | ref_comercial\[i][nombre]    |
| Dirección            | ref_comercial\[i][direccion] |
| Ciudad               | ref_comercial\[i][ciudad]    |
| Teléfono             | ref_comercial\[i][telefono]  |

| Personas de contacto                       | Contenidos arreglo: (1)Nombre (2)Correo (3)Teléfono |
| ------------------------------------------ | --------------------------------------------------- |
| **Etiqueta**                               | **name**                                            |
| Pedidos/facturación                        | facturacion_pedidos[]                               |
| Compras                                    | facturacion_compras[]                               |
| Tesorería                                  | facturacion_tesoreria[]                             |
| Proveedores ejecutivos de cuentas de venta | facturacion_cuentas[]                               |
| Contabilidad/cartera                       | facturacion_contabilidad[]                          |

#### Campos cliente persona natural

| Información personal                                      |                        |
| --------------------------------------------------------- | ---------------------- |
| **Etiqueta**                                              | **name**               |
| Estado civil                                              | estado_civil           |
| Estado civil otro                                         | estado_civil_otro      |
| Profesión, oficio o actividad principal                   | profesion_oficio       |
| Politicamente expuesta                                    | politicamente_expuesta |
| Un familiar directo es una persona políticamente expuesta | familiar_expuesto      |
| En caso afirmativo, informe nombre y parentesco           | familiar_expuesto_info |

| Información tributaria |                        |
| ---------------------- | ---------------------- |
| **Etiqueta**           | **name**               |
| Información tributaria | informacion_tributaria |

| Representación legal                                   |                           |
| ------------------------------------------------------ | ------------------------- |
| **Etiqueta**                                           | **name**                  |
| Nombre completo                                        | rep_legal_nombre_completo |
| Nacionalidad                                           | rep_legal_nacionalidad    |
| Tipo de identificación                                 | rep_legal_tipo_id         |
| Número de identificación                               | rep_legal_numero_id       |
| Fecha y lugar de expedición del documento de identidad | rep_legal_fecha_lugar     |
| Dirección                                              | rep_legal_direccion       |
| Ciudad                                                 | rep_legal_ciudad          |
| Teléfono                                               | rep_legal_telefono        |
| Correo electrónico                                     | rep_legal_correo          |

| Declaración y autorización |                    |
| -------------------------- | ------------------ |
| **Etiqueta**               | **name**           |
| Due diligence              | file_due_diligence |

| Carga de archivos                                | PDF o JPG                   |
| ------------------------------------------------ | --------------------------- |
| **Etiqueta**                                     | **name**                    |
| Fotocopia del RUT                                | file_rut                    |
| Fotocopia documento identidad                    | file_doc_id                 |
| Certificado bancario                             | file_certificado_bancario   |
| Certificaciones comerciales por escrito          | file_certificado_comercial  |
| Certificado de existencia y representación legal | file_certificado_existencia |
| Estados financieros del último año               | file_estados_financieros    |

#### Campos cliente persona jurídica

| Beneficiario o representante legal      |                           |
| --------------------------------------- | ------------------------- |
| **Etiqueta**                            | **name**                  |
| ¿Es Beneficiario o representante legal? | benef_repres_entidad      |
| Razón social de la empresa              | benef_repres_razon_social |
| NIF empresa                             | benef_repres_nif          |
| Porcentaje de participación             | benef_repres_porcentaje   |

| Información tributaria  |                                      |
| ----------------------- | ------------------------------------ |
| **Etiqueta**            | **name**                             |
| Agente retenedor        | info_tributaria_retenedor            |
| Autorretenedor IVA      | info_tributaria_autorretenedor_iva   |
| Nº resolución           | info_tributaria_resolucion_iva       |
| Autorretenedor de renta | info_tributaria_autorretenedor_renta |
| Nº resolucion           | info_tributaria_resolucion_renta     |
| Regimen tributario      | info_tributaria_regimen              |

| Representación legal                                   |                                    |
| ------------------------------------------------------ | ---------------------------------- |
| **Etiqueta**                                           | **name**                           |
| Nombre completo                                        | rep_legal_principal_nombre         |
| Nacionalidad                                           | rep_legal_principal_nacionalidad   |
| Tipo de identificacion                                 | rep_legal_principal_tipo_id        |
| Nº de identificacion                                   | rep_legal_principal_numero_id      |
| Fecha y lugar de expedición del documento de identidad | rep_legal_principal_id_fecha_lugar |
| Dirección                                              | rep_legal_principal_direccion      |
| Ciudad                                                 | rep_legal_principal_ciudad         |
| Teléfono                                               | rep_legal_principal_telefono       |
| Correo electrónico                                     | rep_legal_principal_correo         |
| Nombre completo                                        | rep_legal_suplente_nombre          |
| Nacionalidad                                           | rep_legal_suplente_nacionalidad    |
| Tipo de identificacion                                 | rep_legal_suplente_tipo_id         |
| Nº de identificacion                                   | rep_legal_suplente_numero_id       |
| Fecha y lugar de expedición del documento de identidad | rep_legal_suplente_id_fecha_lugar  |
| Dirección                                              | rep_legal_suplente_direccion       |
| Ciudad                                                 | rep_legal_suplente_ciudad          |
| Teléfono                                               | rep_legal_suplente_telefono        |
| Correo electrónico                                     | rep_legal_suplente_correo          |

| Revisor fiscal                                         | (1: Principal. 2: Suplente. 3:Contador) |
| ------------------------------------------------------ | --------------------------------------- |
| **Etiqueta**                                           | **name**                                |
| Razón social                                           | rev_fiscal_principal_razon_social       |
| Nombre                                                 | rev_fiscal_principal_nombre             |
| Número tarjeta profesional                             | rev_fiscal_principal_numero_tarjeta     |
| Tipo de identificacion                                 | rev_fiscal_principal_tipo_id            |
| Nº de identificacion                                   | rev_fiscal_principal_numero_id          |
| Fecha y lugar de expedición del documento de identidad | rev_fiscal_principal_id_fecha_lugar     |
| Dirección                                              | rev_fiscal_principal_direccion          |
| Ciudad                                                 | rev_fiscal_principal_ciudad             |
| Teléfono                                               | rev_fiscal_principal_telefono           |
| Correo electrónico                                     | rev_fiscal_principal_correo             |
| Razón social                                           | rev_fiscal_suplente_razon_social        |
| Nombre                                                 | rev_fiscal_suplente_nombre              |
| Número tarjeta profesional                             | rev_fiscal_suplente_numero_tarjeta      |
| Tipo de identificacion                                 | rev_fiscal_suplente_tipo_id             |
| Nº de identificacion                                   | rev_fiscal_suplente_numero_id           |
| Fecha y lugar de expedición del documento de identidad | rev_fiscal_suplente_id_fecha_lugar      |
| Dirección                                              | rev_fiscal_suplente_direccion           |
| Ciudad                                                 | rev_fiscal_suplente_ciudad              |
| Teléfono                                               | rev_fiscal_suplente_telefono            |
| Correo electrónico                                     | rev_fiscal_suplente_correo              |
| Razón social                                           | rev_fiscal_contador_razon_social        |
| Nombre                                                 | rev_fiscal_contador_nombre              |
| Número tarjeta profesional                             | rev_fiscal_contador_numero_tarjeta      |
| Tipo de identificacion                                 | rev_fiscal_contador_tipo_id             |
| Nº de identificacion                                   | rev_fiscal_contador_numero_id           |
| Fecha y lugar de expedición del documento de identidad | rev_fiscal_contador_id_fecha_lugar      |
| Dirección                                              | rev_fiscal_contador_direccion           |
| Ciudad                                                 | rev_fiscal_contador_ciudad              |
| Teléfono                                               | rev_fiscal_contador_telefono            |
| Correo electrónico                                     | rev_fiscal_contador_correo              |

| Socios y accionistas           | (Campos dinámicos)                    |
| ------------------------------ | ------------------------------------- |
| **Etiqueta**                   | **name**                              |
| Razón social o nombre completo | socios_accionistas\[i][nombre]        |
| Tipo de identificación         | socios_accionistas\[i][tipo_id]       |
| Número de identificación       | socios_accionistas\[i][numero_id]     |
| Porcentaje de participación    | socios_accionistas\[i][participacion] |

| Carga de archivos                                                                                                                              | PDF o JPG                        |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------- |
| **Etiqueta**                                                                                                                                   | **name**                         |
| Fotocopia del RUT                                                                                                                              | file_rut                         |
| Fotocopia documento identidad representante legal                                                                                              | file_doc_id_representante        |
| Certificado de existencia y representación legal                                                                                               | file_certificado_existencia      |
| Estados financieros firmados por el revisor fiscal y/o contador público del último año con sus notas, fotocopia legible de tarjeta profesional | file_estado_financiero           |
| Fotocopia de declaración de renta de los dos últimos años                                                                                      | file_declaracion_renta           |
| Una certificación bancaria por escrito                                                                                                         | file_certificacion_bancaria      |
| Evidencia de habilitación sanitaria vigente                                                                                                    | file_habilitacion_sanitaria      |
| Certificaciones comerciales por escrito                                                                                                        | file_certificaciones_comerciales |
| Certificado de composición accionaria con identificación de accionistas con participación mayor o igual al 5%                                  | file_certificaciones_accionaria  |
| Certificación firmada de cumplimiento relacionado con el SARLAFT                                                                               | file_certificacion_sarlaft       |

### Proveedores

Los clientes están agrupados en **persona natural** y **persona jurídica**. Cada formulario es independiente pero contiene campos comunes, los que serán marcados en esta documentación en una sección común.

#### Campos proveedores persona natural

| Representación legal                                                       |                        |
| -------------------------------------------------------------------------- | ---------------------- |
| **Etiqueta**                                                               | **name**               |
| Razón social de la empresa que es beneficiario final o representante legal | rep_legal_razon_social |
| NIF de la empresa que es beneficiario final o representante legal          | rep_legal_nif          |
| Porcentaje de participación en la sociedad si es beneficiario final        | rep_legal_porcentaje   |

#### Campos proveedores persona jurídica

| Carga de archivos                                                   | PDF o JPG                    |
| ------------------------------------------------------------------- | ---------------------------- |
| **Etiqueta**                                                        | **name**                     |
| Fotocopia del RUT, no mayor a 60 días                               | file_rut                     |
| Fotocopia documento de identidad                                    | file_doc_id                  |
| Un (1) certificado bancario por escrito                             | file_certificado_bancario    |
| Dos (2) certificaciones comerciales por escrito.                    | file_certificacion_comercial |
| Dos (2) referencias de clientes                                     | file_referencias_clientes    |
| Certificado de existencia y representación legal no mayor a 30 días | file_certificado_existencia  |

#### Campos proveedores persona internacional

| Carga de archivos                                            | PDF o JPG                    |
| ------------------------------------------------------------ | ---------------------------- |
| **Etiqueta**                                                 | **name**                     |
| Identificación empresarial y/o personal                      | file_id                      |
| Fotocopia del documento de identidad del representante legal | file_id_representante        |
| Certificación bancaria con código Swit                       | file_certificacion_bancaria  |
| Certificación comercial (excepto para HCPs)                  | file_certificacion_comercial |
